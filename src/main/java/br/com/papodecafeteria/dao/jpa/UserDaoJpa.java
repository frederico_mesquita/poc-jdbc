package br.com.papodecafeteria.dao.jpa;

import java.io.FileInputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

import br.com.papodecafeteria.dao.model.UserJPA;

public class UserDaoJpa {
	
	private static Logger l = Logger.getLogger(UserJPA.class.getName());

	public static Logger getL() {
		return l;
	}
	
	protected static Properties loadProperties(String pReference){
		Properties properties = null;
		try {
			FileInputStream fileInputStream = new FileInputStream(pReference);
			properties = new Properties();
			properties.load(fileInputStream);
			fileInputStream = null;
		} catch (Exception exc) {
			getL().log(Level.SEVERE, exc.getMessage(), exc);
		}
		return properties;
	}

	public static Connection getConnection(){  
	    Connection conn = null;
	    try{  
	    	Properties properties = loadProperties(UserDaoJpa.class.getProtectionDomain().getCodeSource().getLocation().getPath() + "/connection.property");	    	
	        Class.forName(properties.getProperty("dbclass"));	        
	        String dburl = properties.getProperty("dbjdbctype") + "://" + properties.getProperty("dbaddress") + "/" + 
        					properties.getProperty("dbinstance") + "?useSSL=" + properties.getProperty("dbusessl") + 
        					"&useTimezone=" + properties.getProperty("dbusetimezone") +
        					"&serverTimezone=" + properties.getProperty("dbservertimezone"); 
	        
	        conn = DriverManager.getConnection(dburl, properties.getProperty("userdb"), properties.getProperty("userdbpasswd"));
	        properties = null;
	    } catch(Exception exc){
	    	getL().log(Level.SEVERE, exc.getMessage(), exc);
	    }  
	    return conn;  
	}
	
	public static int save(UserJPA pUser){  
	    int status = 0;  
	    
	    try{
	        PreparedStatement ps = getConnection().prepareStatement(  
	        	"insert into user(name,password,email,sex,country) values(?,?,?,?,?);");  
	        
	        ps.setString(1,pUser.getName());  
	        ps.setString(2,pUser.getPassword());  
	        ps.setString(3,pUser.getEmail());  
	        ps.setString(4,pUser.getSex());  
	        ps.setString(5,pUser.getCountry());  
	        
	        status = ps.executeUpdate();  
	    } catch(Exception exc){
	    	getL().log(Level.SEVERE, exc.getMessage(), exc);
	    }   
	    return status;  
	}
	
	public static int update(UserJPA pUser){  
	    int status = 0;  
	    try{
	        PreparedStatement ps = getConnection().prepareStatement(  
	        	"update user set name=?,email=?,sex=?,country=? where id=?;");
	        
	        ps.setString(1,pUser.getName());
	        ps.setString(2,pUser.getEmail());  
	        ps.setString(3,pUser.getSex());  
	        ps.setString(4,pUser.getCountry());  
	        ps.setInt(5,pUser.getId());  
	        
	        status = ps.executeUpdate();  
	    } catch(Exception exc){
	    	getL().log(Level.SEVERE, exc.getMessage(), exc);
	    }   
	    return status;  
	} 
	
	public static int delete(UserJPA pUser){  
	    int status = 0;  
	    
	    try{
	        PreparedStatement ps = getConnection().prepareStatement("delete from user where id=?;");
	        
	        ps.setInt(1,pUser.getId());  
	        status = ps.executeUpdate();  
	    } catch(Exception exc){
	    	getL().log(Level.SEVERE, exc.getMessage(), exc);
	    }  
	  
	    return status;  
	} 
	
	public static List<UserJPA> getAllRecords(){  
	    List<UserJPA> lstUser = new ArrayList<UserJPA>();  
	      
	    try{
	        PreparedStatement ps = getConnection().prepareStatement("select id, name, email, sex, country from user;");  
	        ResultSet rs = ps.executeQuery(); 
	        
	        while(rs.next()){  
	            UserJPA user=new UserJPA();  
	            
	            user.setId(rs.getInt("id"));  
	            user.setName(rs.getString("name")); 
	            user.setEmail(rs.getString("email"));  
	            user.setSex(rs.getString("sex"));  
	            user.setCountry(rs.getString("country"));  
	            
	            lstUser.add(user);  
	        }  
	    } catch(Exception exc){
	    	getL().log(Level.SEVERE, exc.getMessage(), exc);
	    }  
	    return lstUser;  
	}

	public static UserJPA getRecordById(int pId){  
	    UserJPA user = null;  
	    
	    try{
	        PreparedStatement ps = getConnection().prepareStatement(
	        	"select id, name, email, sex, country from user where id=?;");  
	        ps.setInt(1, pId);  
	        ResultSet rs = ps.executeQuery();  
	        
	        while(rs.next()){  
	            user = new UserJPA();  
	            user.setId(rs.getInt("id"));  
	            user.setName(rs.getString("name")); 
	            user.setEmail(rs.getString("email"));  
	            user.setSex(rs.getString("sex"));  
	            user.setCountry(rs.getString("country"));  
	        }  
	    } catch(Exception exc){
	    	getL().log(Level.SEVERE, exc.getMessage(), exc);
	    }   
	    return user;  
	}
}
